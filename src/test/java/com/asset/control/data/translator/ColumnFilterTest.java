package com.asset.control.data.translator;

import com.google.common.collect.Lists;
import com.asset.control.data.translator.service.transform.ColumnFilter;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ColumnFilterTest {

    @Test
    public void test() throws Exception{
        ColumnFilter columnFilter = new ColumnFilter(Lists.newArrayList("OURID", "OURCOL1", null, "OURCOL3"));

        List<String> filteredRow = columnFilter.filterRow(new String[]{"1", "2", "3", "4"});

        Assert.assertNotNull(filteredRow);
        Assert.assertThat(filteredRow, Matchers.contains("1", "2", "4"));
    }
}
