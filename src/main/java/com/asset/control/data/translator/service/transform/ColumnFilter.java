package com.asset.control.data.translator.service.transform;

import java.util.ArrayList;
import java.util.List;

/**
 * Column filter.
 */
public class ColumnFilter {

    private List<String> filter = new ArrayList<>();

    public ColumnFilter(List<String> filter) {
        this.filter = filter;
    }

    /**
     * Filter row data based on which should be.
     * @param rowValues
     * @return
     */
    public List<String> filterRow(String[] rowValues) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < rowValues.length; i++) {
            if (filter.get(i) != null) {
                result.add(rowValues[i]);
            }
        }
        return result;
    }
}
