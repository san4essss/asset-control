package com.asset.control.data.translator.service.transform;

import com.asset.control.data.translator.service.file.FileReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Header transformer.
 */
public class HeaderTransformer {

    private Map<String, String> headerMap = new HashMap<>();

    private List<String> transformedHeaders = new ArrayList<>();

    public HeaderTransformer(String inputHeaders, InputStream headerTransformerInfo) throws IOException {
        FileReader fileReader = new FileReader(headerTransformerInfo);
        String line = "";
        while ((line = fileReader.getNextLine()) != null) {
            String[] cols = line.split("\t");
            if (cols.length != 2) {
                continue;
            }
            headerMap.put(cols[0], cols[1]);
        }

        transform(inputHeaders);
    }

    /**
     * Return headers mapped based on transformer
     * @return
     */
    public List<String> getTransformedHeaders() {
        return transformedHeaders;
    }

    /**
     * Return result header row
     * @return
     */
    public String getTransformedHeadersRow() {
        return transformedHeaders.stream()
                    .filter(header -> header != null)
                    .collect(Collectors.joining("\t"));
    }

    private void transform(String input) {
        for (String col : input.split("\t")) {
            transformedHeaders.add(headerMap.get(col));
        }
    }
}
