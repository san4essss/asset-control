Data Translator
---------------
Input data from a data vendor needs to be filtered and translated.
Specifications:
1. A data vendor delivery data in flat files. These flat files are in matrix format. The
first row contains the column labels, the further rows are data rows. In the first
column holds a data vendor specific identifier. Column separator is a tab.
Example:
COL0 COL1 COL2 COL3
ID1 VAL11 VAL12 VAL12
ID2 VAL21 VAL22 VAL23
...
2. There is a configuration file that lists the columns that we want to extract. We
want to translate the columns to 'our' names. So this file contains two columns: first
column with original label, second column with 'our' labels.
Example (skip column COL2):
COL0 OURID
COL1 OURCOL1
COL3 OURCOL3
3. There is an another configuration file that lists the data vendor specific identifiers,
so the rows that we want to extract. Similar to point 2: these are translated to the
values in column 2
Example (skip ID1):
ID2 OURIDXXX
4. The task is to build a 'translator' that reads in these three files and produces
output in the same structure: first row with 'our' column labels, further rows with the
data we wanted to extract.
Example based on examples above:
OURID OURCOL1 OURCOL3
OURIDXXX VAL21 VAL23
5. The solution must be able to process very big data files with big number of rows
and columns by utilizing all available resources (CPU cores and memory).
Expected deliveries:
- Working solution written in Java that can be immediately run on production
system. It includes clear instructions how to configure/run the application.
- Complete Java project that includes all necessary sources/resources/build files.
The project is well organized and documented as it would be used later by different
developer to extend the application.

-----------------------------------

Solution:

Application includes embedded HTTP server.

You can build project with maven: mvn clean package.

Run:
1. With Maven. Examples:
    - mvn spring-boot:run
    - mvn spring-boot:run -Dserver.port=8081
    If you want to process very big files specify run option -Drun.jvmArguments=-Xmx1024m (set desired max heap size)
or
2. As jar. Examples:
    - java -jar target/data-translator-0.0.1-SNAPSHOT.jar
    - java -jar target/data-translator-0.0.1-SNAPSHOT.jar --server.port=8081
    If you want to process very big files specify run option -Xmx1024m (set desired max heap size)

Optional parameters to run:
  - server-port: HTTP port to run embedded server (default 8080)
  - spring.http.multipart.maxFileSize: max available size to upload
  - spring.http.multipart.maxRequestSize: max available size of all file to upload per request



Please open in browser <server:port> page, choose files and submit.
After uploading and processing, downloading of transformed file will start.