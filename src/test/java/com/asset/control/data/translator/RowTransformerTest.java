package com.asset.control.data.translator;

import com.asset.control.data.translator.service.transform.RowTransformer;
import com.google.common.collect.Lists;
import com.asset.control.data.translator.service.transform.ColumnFilter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;

public class RowTransformerTest {

    @Test
    public void test() throws Exception{
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("row-transforemer-test/row1.txt").getFile());

        ColumnFilter columnFilter = new ColumnFilter(Lists.newArrayList("OURID", "OURCOL1", null, "OURCOL3"));
        RowTransformer rowTransformer = new RowTransformer(new FileInputStream(file), columnFilter);

        Assert.assertEquals(null, rowTransformer.transformRow("ID1\tVAL11\tVAL12\tVAL12"));
        Assert.assertEquals("OURIDXXX\tVAL21\tVAL23", rowTransformer.transformRow("ID2\tVAL21\tVAL22\tVAL23"));
    }
}
