package com.asset.control.data.translator.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Transformer service.
 */
public interface TransformService {

    /**
     * Read input stream, transform it and write to output stream
     * @param inputFile
     * @param outputStream
     * @param headerTransformInfo
     * @param rowTransformInfo
     * @throws IOException
     */
    void transformFile(
            InputStream inputFile,
            OutputStream outputStream,
            InputStream headerTransformInfo,
            InputStream rowTransformInfo) throws IOException;
}
