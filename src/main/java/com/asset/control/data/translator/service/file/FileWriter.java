package com.asset.control.data.translator.service.file;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Synchronized input stream reader.
 */
public class FileWriter {

    private BufferedWriter bufferedWriter;

    public FileWriter(OutputStream outputStream) {
        this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
    }

    /**
     * Thread safe write line to stream.
     * @param s
     * @throws IOException
     */
    public synchronized void saveLine(String s) throws IOException {
        if (s != null) {
            bufferedWriter.write(s);
            bufferedWriter.newLine();
        }
    }

    public void flush() throws IOException {
        bufferedWriter.flush();
    }
}
