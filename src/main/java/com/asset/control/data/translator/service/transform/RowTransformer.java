package com.asset.control.data.translator.service.transform;

import com.asset.control.data.translator.service.file.FileReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Row transformer.
 */
public class RowTransformer {

    private ColumnFilter columnFilter;
    private Map<String, String> idMap = new HashMap<>();

    public RowTransformer(InputStream rowTransformerInfo, ColumnFilter columnFilter) throws IOException {
        FileReader fileReader = new FileReader(rowTransformerInfo);
        String line = "";
        while ((line = fileReader.getNextLine()) != null) {
            String[] cols = line.split("\t");
            if (cols.length != 2) {
                continue;
            }
            idMap.put(cols[0], cols[1]);
        }

        this.columnFilter = columnFilter;
    }

    /**
     * Transform row using column filter and id map
     * @param row
     * @return
     */
    public String transformRow(String row) {
        String[] rowData = row.split("\t");
        String newId = idMap.get(rowData[0]);
        if (newId == null) {
            return null;
        }

        rowData[0] = newId;

        List<String> filteredRow = columnFilter.filterRow(rowData);
        return String.join("\t", filteredRow);
    }
}
