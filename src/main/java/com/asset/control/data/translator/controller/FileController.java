package com.asset.control.data.translator.controller;

import com.asset.control.data.translator.service.TransformServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/files")
public class FileController {

    @Autowired
    private TransformServiceImpl fileService;

    @RequestMapping(value="/transform", method=RequestMethod.POST)
    public void transform(
            @RequestParam("inputFile") MultipartFile inputFile,
            @RequestParam("headerMapFile") MultipartFile headerTransformer,
            @RequestParam("idMapFile") MultipartFile rowTransformer,
            HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=output.txt");

        fileService.transformFile(
                inputFile.getInputStream(),
                response.getOutputStream(),
                headerTransformer.getInputStream(),
                rowTransformer.getInputStream());

        response.flushBuffer();
    }
}
