package com.asset.control.data.translator;

import com.asset.control.data.translator.service.transform.HeaderTransformer;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;

public class HeaderTransformerTest {

    @Test
    public void test() throws Exception{
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("header-transforemer-test/header1.txt").getFile());

        HeaderTransformer headerTransformer = new HeaderTransformer("COL0\tCOL1\tCOL2\tCOL3", new FileInputStream(file));

        Assert.assertEquals("OURID\tOURCOL1\tOURCOL3", headerTransformer.getTransformedHeadersRow());
        Assert.assertThat(headerTransformer.getTransformedHeaders(), Matchers.contains("OURID", "OURCOL1", null, "OURCOL3"));
    }
}
