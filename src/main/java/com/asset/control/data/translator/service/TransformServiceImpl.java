package com.asset.control.data.translator.service;

import com.asset.control.data.translator.service.file.FileReader;
import com.asset.control.data.translator.service.file.FileWriter;
import com.asset.control.data.translator.service.transform.ColumnFilter;
import com.asset.control.data.translator.service.transform.HeaderTransformer;
import com.asset.control.data.translator.service.transform.RowTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Transformer service.
 */
@Service
public class TransformServiceImpl implements TransformService {

    public static final Logger LOG = LoggerFactory.getLogger(TransformServiceImpl.class);

    @Override
    public void transformFile(
            InputStream inputFile,
            OutputStream outputStream,
            InputStream headerTransformInfo,
            InputStream rowTransformInfo) throws IOException {

        FileReader fileReader = new FileReader(inputFile);
        FileWriter fileWriter = new FileWriter(outputStream);

        //transform headers
        HeaderTransformer headerTransformer = new HeaderTransformer(fileReader.getNextLine(), headerTransformInfo);
        fileWriter.saveLine(headerTransformer.getTransformedHeadersRow());
        ColumnFilter columnFilter = new ColumnFilter(headerTransformer.getTransformedHeaders());

        //Prepare row transformer
        RowTransformer rowTransformer = new RowTransformer(rowTransformInfo, columnFilter);

        doMultiThreadProcessing(fileReader, fileWriter, rowTransformer);
    }

    private void doMultiThreadProcessing(
            FileReader fileReader,
            FileWriter fileWriter,
            RowTransformer rowTransformer) throws IOException {
        int cores = Runtime.getRuntime().availableProcessors();

        ExecutorService executor = Executors.newFixedThreadPool(cores);
        for (int i = 0; i< cores; i++) {
            FileTransformer fileTransformer = new FileTransformer(fileReader, fileWriter, rowTransformer);
            Thread thread = new Thread(fileTransformer);
            executor.submit(thread);
        }

        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            LOG.error("Thread pool executor was interrupted while waiting", e);
        }

        fileWriter.flush();
    }

    private class FileTransformer implements Runnable {

        private FileReader fileReader;
        private FileWriter fileWriter;
        private RowTransformer rowTransformer;

        FileTransformer(
                FileReader fileReader,
                FileWriter fileWriter,
                RowTransformer rowTransformer) {
            this.fileReader = fileReader;
            this.fileWriter = fileWriter;
            this.rowTransformer = rowTransformer;
        }

        @Override
        public void run() {
            String row = null;
            do {
                try {
                    row = fileReader.getNextLine();
                    if (row != null) {
                        fileWriter.saveLine(rowTransformer.transformRow(row));
                    }
                } catch (IOException e) {
                    LOG.error("Unexpected IO error (row=" + row, e);
                }
            } while (row != null && !Thread.currentThread().isInterrupted());
        }
    }
}
