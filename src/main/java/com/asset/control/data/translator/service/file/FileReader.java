package com.asset.control.data.translator.service.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Synchronized input stream reader.
 */
public class FileReader {

    private BufferedReader bufferedReader;

    public FileReader(InputStream inputStream) {
        this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    /**
     * Thread safe reading next line from stream.
     * @return
     * @throws IOException
     */
    public synchronized String getNextLine() throws IOException {
        return bufferedReader.readLine();
    }
}
